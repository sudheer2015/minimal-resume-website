# Minimal resume website template

This website is a re-designed from Nicolas Meuzard's Minimal-resume work. The original work can be found at [onepage love](https://onepagelove.com/minimal-resume) and the demo at [oenpage love demo](https://demos.onepagelove.com/html/minimal-resume/) website.

## Project directory structure

> tree --dirsfirst

```
.
├── images
│   ├── github.jpg
│   ├── gitlab.png
│   ├── headshot.jpeg
│   ├── intel_devmesh.png
│   └── linkedin.png
├── index.html
├── Readme.md
└── styles.css
```


**Note:** The licensing permissions applicable for the original template is also applicable to this one. Please check the original developer license terms before using this for your own or commercial work.


*If you re-adopt this templete please consider attributing us to your work.*

<br>

> If you like his work, please consider making a small donation to his work. More information is available on the *oenpagelove* website

<br>

**Thank you. Have a good day!**
